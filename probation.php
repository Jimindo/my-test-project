<?php
$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '1111';
$dbname = 'test';
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

$sql = "SELECT tu.last_name, 
       tu.first_name, 
       tu.middle_name, 
       tp.name, 
        IF(tud.created_at IS NOT NULL AND tud.is_active = 0, DATE(tud.created_at), 'Не уволен'),
        IF(tud.reason_id IS NOT NULL AND tud.is_active = 0, tdr.description, 'Не уволен'),
       tp.salary,
        DATE(tu.created_at) FROM testdb_user as tu
        LEFT JOIN `testdb_user_position` tup ON tu.id = tup.user_id
        LEFT JOIN `testdb_position` tp ON tup.position_id = tp.id
        LEFT JOIN `testdb_user_dismission` tud ON tu.id = tud.user_id
        LEFT JOIN `testdb_dismission_reason` tdr ON tud.reason_id = tdr.id
        WHERE DATEDIFF(CURRENT_DATE, DATE(tu.created_at))<=90
        ORDER BY tu.last_name;";

if($result = mysqli_query($conn, $sql)) {;
    //$rowsCount = mysqli_num_rows($result); //количество полученных строк
    //$page_elem = 7; Кол-во элементов выводимых на одной странице таблицы

    echo "<table id = domainsTable  class= table border = 2 align = center>
          <caption>Сотрудники на испытательном сроке</caption>
          <thead>
          <tr>
          <th>Фамилия</th>
          <th>Имя</th>
          <th>Отчество</th>
          <th>Должность</th>
          <th>Дата увольнения</th>
          <th>Причина увольнения</th>
          <th>Зарплата</th>
          <th>Дата трудостройства</th></tr></thead>";
    foreach ($result as $row) {
        echo "<tbody>";
        echo "<tr>";
          echo "<td>" . $row["last_name"] . "</td>";
          echo "<td>" . $row["first_name"] . "</td>";
          echo "<td>" . $row["middle_name"] . "</td>";
          echo "<td>" . $row["name"] . "</td>";
          echo "<td>" . $row["IF(tud.created_at IS NOT NULL AND tud.is_active = 0, DATE(tud.created_at), 'Не уволен')"] . "</td>";
          echo "<td>" . $row["IF(tud.reason_id IS NOT NULL AND tud.is_active = 0, tdr.description, 'Не уволен')"] . "</td>";
          echo "<td>" . $row["salary"] . "</td>";
          echo "<td>" . $row["DATE(tu.created_at)"] . "</td>";
        echo "</tr></tbody>";
  }
  echo "</table>";
  //for($i=1; $i<=($rowsCount/$page_elem); $i++){
  //    echo "<a href='index.php?page=".$i."'>".$i."</a>";
  //} Цикл был нужен для пагинации
  mysqli_free_result($result);
} else {
   echo "Ошибка: " . mysqli_error($conn);
}

mysqli_close($conn);