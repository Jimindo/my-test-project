<?php
$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '1111';
$dbname = 'test';
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

$sql = "SELECT tu.last_name, 
        tu.first_name, 
        tu.middle_name, 
        tp.name, 
        DATE(tud.created_at),
        tdr.description,
        tp.salary
        FROM testdb_user as tu
        LEFT JOIN `testdb_user_position` tup ON tu.id = tup.user_id
        LEFT JOIN `testdb_position` tp ON tup.position_id = tp.id
        LEFT JOIN `testdb_user_dismission` tud ON tu.id = tud.user_id
        LEFT JOIN `testdb_dismission_reason` tdr ON tud.reason_id = tdr.id
        WHERE tud.is_active = 0
        ORDER BY tu.last_name;";

if($result = mysqli_query($conn, $sql)) {;
    $rowsCount = mysqli_num_rows($result); //количество полученных строк
    echo "<table border = 2 align = center><caption>Уволенные сотрудники</caption><tr><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Должность</th><th>Дата увольнения</th><th>Причина увольнения</th><th>Зарплата</th></tr>";
    foreach ($result as $row) {
        echo "<tr>";
        echo "<td>" . $row["last_name"] . "</td>";
        echo "<td>" . $row["first_name"] . "</td>";
        echo "<td>" . $row["middle_name"] . "</td>";
        echo "<td>" . $row["name"] . "</td>";
        echo "<td>" . $row["DATE(tud.created_at)"] . "</td>";
        echo "<td>" . $row["description"] . "</td>";
        echo "<td>" . $row["salary"] . "</td>";
        echo "</tr>";
    }
    echo "</table>";
    mysqli_free_result($result);
} else {
    echo "Ошибка: " . mysqli_error($conn);
}

mysqli_close($conn);
