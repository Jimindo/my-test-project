<?php
$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '1111';
$dbname = 'test';
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

$sql = "SELECT tu.last_name,
       tu.first_name,
       tu.middle_name,
       td.name as department_name,
       tp.name as position_name,
       tup.created_at
        
        FROM `testdb_user_position` as tup
        JOIN (SELECT tup1.department_id, MAX(tup1.created_at) as max_created_at FROM `testdb_user_position` as tup1 
                                
                                GROUP BY tup1.department_id) as tup1 ON tup.department_id = tup1.department_id AND tup.created_at = tup1.max_created_at
        JOIN `testdb_user` tu ON tup.user_id = tu.id
        JOIN `testdb_department` td ON tup.department_id = td.id
        JOIN `testdb_position` tp ON tup.position_id = tp.id
        ORDER BY tup.department_id";


if($result = mysqli_query($conn, $sql)) {;
    $rowsCount = mysqli_num_rows($result); //количество полученных строк
    echo "<table border = 2 align = center><caption>Последние нанятые сотрудники</caption><tr>
    <th>Фамилия</th>
    <th>Имя</th>
    <th>Отчество</th>
    <th>Департамент</th>
    <th>Должность</th>
    <th>Дата трудоустройства</th></tr>";
    foreach ($result as $row) {
        echo "<tr>";
        echo "<td>" . $row["last_name"] . "</td>";
        echo "<td>" . $row["first_name"] . "</td>";
        echo "<td>" . $row["middle_name"] . "</td>";
        echo "<td>" . $row["department_name"] . "</td>";
        echo "<td>" . $row["position_name"] . "</td>";
        echo "<td>" . $row["created_at"] . "</td>";
        echo "</tr>";
    }
    echo "</table>";
    mysqli_free_result($result);
} else {
    echo "Ошибка: " . mysqli_error($conn);
}

mysqli_close($conn);